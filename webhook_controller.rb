class WebhookController < ApplicationController
  skip_before_action :verify_authenticity_token

  def query_bot(session_id, text)
    query_input = { text: { text: text, language_code: 'en'}}
    bot_session = @dialogflow.class.session_path ENV['GOOGLE_PROJECT_ID'], session_id
    response = @dialogflow.detect_intent bot_session, query_input
    query_result = response.query_result
    end_conversation = JSON.parse(query_result.to_json).fetch('diagnosticInfo', {})['end_conversation']
    return query_result.fulfillment_text, end_conversation
  end
end